const gulp = require("gulp");
const plugins = require("gulp-load-plugins")();
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const bs = require("browser-sync");
const cp = require("child_process");
const electron = require("electron-prebuilt");

let BROWSER_SYNC_CLIENT_URL;

gulp.task("templates", () => {

  gulp.src("src/templates/index.pug")
    .pipe(plugins.pug({
      pretty: true
    }))
    .pipe(gulp.dest("dist"));

});

gulp.task("styles", () => {

  gulp.src("src/styles/index.styl")
    .pipe(plugins.stylus({
      compress: false
    }))
    .pipe(gulp.dest("dist"));

});

gulp.task("scripts", () => {

  browserify("src/scripts/index.js", { debug: true })
    .exclude("electron")
    .transform("babelify", { presets: ["es2015", "react"] })
    .transform("uglifyify", { global: true })
    .bundle()
    .pipe(source("index.js"))
    .pipe(gulp.dest("dist"));

});

gulp.task("assets", () => {

  gulp.src("src/assets/**/*")
    .pipe(gulp.dest("dist"));

});

gulp.task("build", ["templates", "scripts", "styles", "assets"]);

gulp.task("electron", ["build", "browser-sync"], () => {
  return cp.spawn(electron, ["dist/electron.js"], {
    env: {
      BROWSER_SYNC_CLIENT_URL
    }
  });
});

gulp.task("browser-sync", () => {
  bs.init({
    ui: false,
    // Port 35829 = LiveReload's default port 35729 + 100.
    // If the port is occupied, Browsersync uses next free port automatically.
    port: 35829,
    ghostMode: false,
    open: false,
    notify: false,
    logSnippet: false,
    server: {
      baseDir: "dist"
    },
    scriptPath(path, port, options) {
      const scriptPath = options.get("absolute");
      BROWSER_SYNC_CLIENT_URL = scriptPath;
      return scriptPath;
    }
  });
});

gulp.task("watch", ["electron"], () => {

  gulp.watch(["src/templates/**/*.pug"], ["templates", bs.reload]);
  gulp.watch(["src/scripts/**/*.js"], ["scripts", bs.reload]);
  gulp.watch(["src/styles/**/*.styl"], ["styles", bs.reload]);

});

gulp.task("default", ["watch"]);
